const BASEURL = 'https://api.virapi.com/vir_github1be479fgc18h9/demo/'

export default {
  goodsList: `${BASEURL}goodsList`, // 首页-商品列表
  my: `${BASEURL}my`, // 我的-查询个人信息
}
import { Component } from 'react'
import './app.scss'
import 'taro-ui/dist/style/index.scss'

class App extends Component {

  componentDidMount () {}

  componentDidShow () {}

  componentDidHide () {}

  componentDidCatchError () {}

  // this.props.children 是将要会渲染的页面1111122223334444
  render () {
    return this.props.children
  }
}

export default App
